"""Centralizes functions operations related to update security group"""

import logging
import boto3

EC2_CLIENT = boto3.client("ec2")

LOGGER = logging.getLogger()


def add_inbound_rule(security_group, ip_addr, ip_protocol, port):
    """Create an inbound rule in the specified security group"""

    ip_addr_range = ip_addr + "/32"
    rule_desc = "Rule automatically created by the Lambda function"

    data = EC2_CLIENT.authorize_security_group_ingress(
        GroupId=security_group,
        IpPermissions=[
            {
                "IpProtocol": ip_protocol,
                "FromPort": port,
                "ToPort": port,
                "IpRanges": [
                    {"CidrIp": ip_addr_range, "Description": rule_desc}
                ],
            }
        ],
    )

    LOGGER.debug("Ingress rule data: %s", data)
