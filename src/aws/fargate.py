"""Fargate module centralizes functions related to AWS Fargate operations"""

import logging
import boto3

ECS_CLIENT = boto3.client("ecs")
EC2_CLIENT = boto3.client("ec2")

LOGGER = logging.getLogger()


def count_tasks_running(cluster_name, task_definition):
    """
    Count existing tasks having the same task definition as the specified
    """

    response = ECS_CLIENT.list_tasks(
        cluster=cluster_name, family=_extract_family_name(task_definition)
    )

    task_count = response.get("taskArns", [])

    return len(task_count)


def _extract_family_name(task_defintion):
    """Extracts the family name from the task definition"""

    family_name = task_defintion.rsplit(":", 1)[0]
    return family_name


def start_task(
    cluster_name, task_definition, subnet, security_group, started_by
):
    """Logic for starting a new Fargate task"""

    response = ECS_CLIENT.run_task(
        cluster=cluster_name,
        taskDefinition=task_definition,
        startedBy=started_by,
        networkConfiguration={
            "awsvpcConfiguration": {
                "subnets": [subnet],
                "securityGroups": [security_group],
                "assignPublicIp": "ENABLED",
            }
        },
    )

    return response["tasks"][0]["containers"][0]["taskArn"]


def wait_task_running(cluster_name, task_arn):
    """Logic for waiting for the task to be in running state"""

    waiter = ECS_CLIENT.get_waiter("tasks_running")
    waiter.wait(cluster=cluster_name, tasks=[task_arn])


def fetch_task_container_ip(cluster_name, task_arn):
    """
    Fetch the container's ip of an specified task

    This function prioritize to fetch the external ip but if this does not
    exist, it will return the private ip.
    """

    private_ip = None
    public_ip = None

    response = ECS_CLIENT.describe_tasks(
        cluster=cluster_name, tasks=[task_arn]
    )

    eni = None
    for detail in response["tasks"][0]["attachments"][0]["details"]:
        if detail["name"] == "networkInterfaceId":
            eni = detail["value"]
        elif detail["name"] == "privateIPv4Address":
            private_ip = detail["value"]

    LOGGER.debug("ENI: %s, private ip: %s", eni, private_ip)

    response = EC2_CLIENT.describe_network_interfaces(
        NetworkInterfaceIds=[eni]
    )

    public_ip = (
        response["NetworkInterfaces"][0].get("Association").get("PublicIp")
    )

    LOGGER.debug("Public ip: %s", public_ip)

    return public_ip if public_ip else private_ip
