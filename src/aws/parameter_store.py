"""Centralizes operations to AWS Parameter Store"""

import json
import logging
import boto3

SSM_CLIENT = boto3.client("ssm")

LOGGER = logging.getLogger()


def load_from_parameter_store(ssm_parameter_name):
    """
    Load values from parameter store.

    It is expected the value stored in SSM to be in JSON format. This function
    will parse the JSON into a dict and return it
    """
    config_values = {}

    param_details = SSM_CLIENT.get_parameter(
        Name=ssm_parameter_name, WithDecryption=True
    )

    param = param_details.get("Parameter", {"Value": "{}"})
    config_values = json.loads(param.get("Value"))

    LOGGER.debug("Found parameter: %s", str(config_values))

    return config_values
