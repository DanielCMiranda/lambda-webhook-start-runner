"""Lambda function to start the Runner manager triggered by GitLab Webhook"""

import json
import logging
import os

from aws.fargate import (
    count_tasks_running,
    fetch_task_container_ip,
    start_task,
    wait_task_running,
)
from aws.parameter_store import load_from_parameter_store
from aws.security_group import add_inbound_rule
from config.logger import configure_logger

SSM_PARAMETER_NAME = os.getenv("SSM_PARAMETER_NAME", "lambda-gitlab-runner")

LOGGER = logging.getLogger()


def lambda_handler(evt, _):
    """Entry point for the Lambda function"""

    configure_logger()

    func_config = load_from_parameter_store(SSM_PARAMETER_NAME)

    if not _is_authenticated(evt, func_config.get("gitlabHeaderToken")):
        return {"statusCode": 403}

    LOGGER.info("Starting processing the request")

    response_body = _process_request(
        func_config["clusterName"],
        func_config["subnet"],
        func_config["securityGroup"],
        func_config["runnerTaskDefinition"],
    )

    LOGGER.info("Finished processing the request")

    return {"statusCode": 200, "body": json.dumps(response_body)}


def _is_authenticated(evt, expected_header_token):
    """
    Process authentication based on an expected header information

    This function checks if the "X-Gitlab-Token" header is filled with
    the value presented in the function configuration (SSM parameter).
    """

    custom_headers = evt.get("headers", {})
    token = custom_headers.get("X-Gitlab-Token", None)

    return token == expected_header_token


def _process_request(cluster_name, subnet, security_group, task_definition):
    """Process the Lambda function request"""

    message = None

    task_count = count_tasks_running(cluster_name, task_definition)

    if task_count == 0:

        task_arn = _run_task(
            cluster_name, task_definition, subnet, security_group
        )

        _create_ssh_inbound_rule(cluster_name, security_group, task_arn)

        message = "Task successfully created"

    else:
        LOGGER.info("Task already exist, will abort")
        message = "Task already exists in the cluster"

    return {"message": message}


def _run_task(cluster_name, task_definition, subnet, security_group):
    """Trigger the creation of a new Runner manager task"""

    LOGGER.info("Will start a new task")

    started_by = "lambda-webhook-function"

    task_arn = start_task(
        cluster_name, task_definition, subnet, security_group, started_by
    )

    LOGGER.debug("Will wait task to be in Running state")

    wait_task_running(cluster_name, task_arn)

    LOGGER.debug("Task started with success")

    return task_arn


def _create_ssh_inbound_rule(cluster_name, security_group, task_arn):
    """Register an inbound rule to allow SSH connections from the Runner"""

    LOGGER.info("Will register inbound rule to allow SSH from Runner")

    ip_address = fetch_task_container_ip(cluster_name, task_arn)
    add_inbound_rule(security_group, ip_address, "tcp", 22)

    LOGGER.info("Inbound rule created")
