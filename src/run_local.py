"""Start point to run the function locally"""

from aws.parameter_store import load_parameter
from handler import lambda_handler, SSM_PARAMETER_NAME


if __name__ == "__main__":
    EVENT = {
        "headers": {
            "X-Gitlab-Token": load_parameter(SSM_PARAMETER_NAME)[
                "gitlabPrivateToken"
            ]
        }
    }
    lambda_handler(EVENT, None)
