#!/usr/bin/make -f
# -*- makefile -*-

PYTHONPATH := ${CURDIR}
export PYTHONPATH

export env $(cat .env)


all: help
help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""
	@echo "   1. make clean                             - Clean all pyc and caches"
	@echo "   2. make black                             - Run black"
	@echo "   3. make check_black                       - Run check for black"
	@echo "   4. make lint                              - Run lint report"
	@echo "   5. make deploy function_name=myFunction   - Deploy the Lambda function"
	@echo "   6. make tests                             - Run unit tests"
	@echo ""
	@echo ""


.PHONY: clean
clean:
	@echo "Clean files pyc and caches..."
	rm -rf build/ dist/ docs/_build *.egg-info .coverage
	find $(CURDIR) -name "*.py[co]" -delete
	find $(CURDIR) -name "*.orig" -delete
	find $(CURDIR)/$(MODULE) -name "__pycache__" | xargs rm -rf
	find $(CURDIR)/$(MODULE) -name ".pytest_cache" | xargs rm -rf

.PHONY: black
black:
	pipenv run black -l 79 -t py37 .

.PHONY: check_black
check_black:
	pipenv run black -l 79 -t py37 . --check

.PHONY: lint
lint:
	find . -iname "*.py" | grep -v __pycache__ | grep -v .history | xargs pipenv run pylint

deploy_lambda:
	sam package --template-file template.yml --output-template-file package.yml --s3-bucket ${s3_bucket}
	sam deploy --template-file package.yml --stack-name ${stack_name} --capabilities CAPABILITY_IAM

.PHONY: deploy
deploy: clean deploy_lambda

.PHONY: tests
tests:
	pipenv run pytest --cov-config=.coveragerc --cov=src
