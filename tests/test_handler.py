"""Tests for the handler"""

from unittest.mock import patch
import pytest

with patch("boto3.client"):
    from handler import lambda_handler

FUNC_CONFIG = {
    "clusterName": "my-cluster",
    "subnet": "subnet-XYZ",
    "securityGroup": "sg-XYZ",
    "runnerTaskDefinition": "my-task-def",
    "gitlabHeaderToken": "expected-token",
}


@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@pytest.mark.parametrize(
    "ssm_param", [{"gitlabHeaderToken": "wrong-token"}, {}]
)
def test_lambda_handler_when_not_authenticated(mocked_load_params, ssm_param):
    """Test"""

    mocked_load_params.return_value = ssm_param

    evt = {"headers": {"X-Gitlab-Token": FUNC_CONFIG["gitlabHeaderToken"]}}
    result = lambda_handler(evt, None)

    mocked_load_params.assert_called_once()
    assert result["statusCode"] == 403


# pylint: disable=too-many-arguments
@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@patch("handler.count_tasks_running", return_value=0)
@patch("handler.start_task", return_value="fake-task-arn")
@patch("handler.wait_task_running")
@patch("handler.fetch_task_container_ip", return_value="10.0.0.1")
@patch("handler.add_inbound_rule")
def test_lambda_handler_when_starting_runner_successfully(
    mocked_load_params,
    mocked_count_tasks,
    mocked_start_task,
    mocked_wait_task,
    mocked_fetch_ip,
    mocked_add_rule,
):
    """Test"""

    evt = {"headers": {"X-Gitlab-Token": FUNC_CONFIG["gitlabHeaderToken"]}}

    result = lambda_handler(evt, None)

    mocked_load_params.assert_called_once()
    mocked_count_tasks.assert_called_once()
    mocked_start_task.assert_called_once()
    mocked_wait_task.assert_called_once()
    mocked_fetch_ip.assert_called_once()
    mocked_add_rule.assert_called_once()
    assert result["statusCode"] == 200
    assert "Task successfully created" in result["body"]


@patch("handler.load_from_parameter_store", return_value=FUNC_CONFIG)
@patch("handler.count_tasks_running", return_value=1)
def test_lambda_handler_when_runner_already_up(
    mocked_load_params, mocked_count_tasks
):
    """Test"""

    evt = {"headers": {"X-Gitlab-Token": FUNC_CONFIG["gitlabHeaderToken"]}}

    result = lambda_handler(evt, None)

    mocked_load_params.assert_called_once()
    mocked_count_tasks.assert_called_once()
    assert result["statusCode"] == 200
    assert "Task already exists in the cluster" in result["body"]
