"""Tests for the parameter store module"""

from unittest.mock import patch
from aws.parameter_store import load_from_parameter_store


@patch(
    "aws.parameter_store.SSM_CLIENT.get_parameter",
    return_value={"Parameter": {"Value": '{"clusterName": "my-cluster"}'}},
)
def test_load_from_parameter_store(mocked_ssm_get_param):
    """Test"""

    obj = load_from_parameter_store("parameter-name")
    mocked_ssm_get_param.assert_called_once()

    assert obj == {"clusterName": "my-cluster"}
