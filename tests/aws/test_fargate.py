"""Tests for the fargate module"""

from unittest.mock import patch
import pytest
from aws.fargate import (
    count_tasks_running,
    start_task,
    wait_task_running,
    fetch_task_container_ip,
)

MOCKED_ECS_DESCRIBE_TASKS_API_RESULT = {
    "tasks": [
        {
            "attachments": [
                {
                    "details": [
                        {"name": "networkInterfaceId", "value": "task-eni"},
                        {"name": "privateIPv4Address", "value": "192.168.0.0"},
                    ]
                }
            ]
        }
    ]
}


@patch("aws.fargate.ECS_CLIENT.list_tasks")
@pytest.mark.parametrize(
    "api_result, expected_count",
    [
        ({"taskArns": ["arn1", "arn2"]}, 2),
        ({"taskArns": ["arn1"]}, 1),
        ({"taskArns": []}, 0),
    ],
)
def test_count_tasks_running(
    mocked_ecs_list_tasks, api_result, expected_count
):
    """Test"""

    mocked_ecs_list_tasks.return_value = api_result

    count = count_tasks_running("my-cluster", "task-def")

    mocked_ecs_list_tasks.assert_called_once()
    assert count == expected_count


@patch("aws.fargate.ECS_CLIENT.list_tasks")
@pytest.mark.parametrize(
    "task_definition, family_name",
    [
        ("task-def", "task-def"),
        ("task-def:1", "task-def"),
        ("task-def:9999", "task-def"),
    ],
)
def test_count_tasks_running_should_properly_parse_family_name(
    mocked_ecs_list_tasks, task_definition, family_name
):
    """Test"""

    count_tasks_running("my-cluster", task_definition)

    mocked_ecs_list_tasks.assert_called_once_with(
        cluster="my-cluster", family=family_name
    )


@patch(
    "aws.fargate.ECS_CLIENT.run_task",
    return_value={"tasks": [{"containers": [{"taskArn": "my-task-arn"}]}]},
)
def test_start_task(mocked_ecs_run_task):
    """Test"""

    arn = start_task(
        "my-cluster", "task-def", "subnet", "sec-group", "started-by"
    )

    mocked_ecs_run_task.assert_called_once()
    assert arn == "my-task-arn"


@patch("aws.fargate.ECS_CLIENT.get_waiter")
def test_wait_task_running(mocked_ecs_get_waiter):
    """Test"""

    cluster_name = "my-cluster"
    task_arn = "task-arn"

    wait_task_running(cluster_name, task_arn)

    mocked_ecs_get_waiter.return_value.wait.assert_called_once_with(
        cluster=cluster_name, tasks=[task_arn]
    )


@patch(
    "aws.fargate.ECS_CLIENT.describe_tasks",
    return_value=MOCKED_ECS_DESCRIBE_TASKS_API_RESULT,
)
@patch(
    "aws.fargate.EC2_CLIENT.describe_network_interfaces",
    return_value={
        "NetworkInterfaces": [{"Association": {"PublicIp": "10.0.0.1"}}]
    },
)
def test_fetch_task_container_ip_when_exist_public_ip(
    mocked_ecs_desc_tasks, mocked_ec2_desc_net
):
    """Test"""

    ip_add = fetch_task_container_ip("custer-name", "task-arn")

    mocked_ecs_desc_tasks.assert_called_once()
    mocked_ec2_desc_net.assert_called_once()
    assert ip_add == "10.0.0.1"


@patch(
    "aws.fargate.ECS_CLIENT.describe_tasks",
    return_value=MOCKED_ECS_DESCRIBE_TASKS_API_RESULT,
)
@patch(
    "aws.fargate.EC2_CLIENT.describe_network_interfaces",
    return_value={"NetworkInterfaces": [{"Association": {"PublicIp": ""}}]},
)
def test_fetch_task_container_ip_when_no_public_ip_exist(
    mocked_ecs_desc_tasks, mocked_ec2_desc_net
):
    """Test"""

    ip_add = fetch_task_container_ip("custer-name", "task-arn")

    mocked_ecs_desc_tasks.assert_called_once()
    mocked_ec2_desc_net.assert_called_once()
    assert ip_add == "192.168.0.0"
