"""Tests for the security_group module"""

from unittest.mock import patch
from aws.security_group import add_inbound_rule


@patch("aws.security_group.EC2_CLIENT")
def test_add_inbound_rule(mocked_client):
    """Test"""

    add_inbound_rule("security_group", "192.168.0.1", "tcp", 22)
    mocked_client.authorize_security_group_ingress.assert_called_once()
